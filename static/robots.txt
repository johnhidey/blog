User-agent: *
Allow: /

Sitemap: https://smallmedium.net/sitemap.xml

Disallow: /categories/
Disallow: /tags/
Disallow: /webfonts/
Disallow: /search/
Disallow: /page/1/
