# Blog

A simple blog using [hugo][hugo] [SSG][ssg] and a custom
theme [nina][nina].

## Installation

In order to write post and see it locally before pushing into the
production environment, you will need to have `hugo` installed.

To install hugo, follow along the installation instructions provided
[here][install].

## Usage

To create new content for the blog, you will need to create a markdown
file for post.  To create your post, issue the following command.

```bash
hugo new blog/name-of-article.md
```

This will then create a file named `name-of-article.md` in the directory
`content/blog/`  Not, that the directory specified when creating the
content did not contain `content`, this is not a mistake.

Add your content to the file you just created and then start up the
hugo server to view your converted markdown.

To start the `hugo server`, Ensure that you have [hugo][hugo] installed
then issue the following command to build all of markdown pages into
and complete html site.

## Development mode locally
```bash

# Does not render drafts
$ hugo server

# Render the drafts for testing, a post can be marked a draft in the
# post front-matter.
$ hugo server -D 
```

## License
[MIT][MIT]

## Badges
[![wakatime](https://wakatime.com/badge/gitlab/johnhidey/blog.svg)](https://wakatime.com/badge/gitlab/johnhidey/blog)

[nina]: https://gitlab.com/johnhidey/nina
[hugo]: https://gohugo.io/
[ssg]: https://staticsitegenerators.net/
[install]: https://gohugo.io/getting-started/installing/
[MIT]: https://gitlab.com/johnhidey/blog/-/raw/master/LICENSE
