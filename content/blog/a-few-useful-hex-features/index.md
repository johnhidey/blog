---
title: "A few useful features of hex"
author: "John Hidey"
date: 2024-11-14
tags: ["hex", "elixir"]
---
Hex is a package manager for the BEAM ecosystem.  Any language capable of running on the BEAM can utilize the hex package manager.  Hex, just like any package manager can add/update/remove packages from a project.  Hex also has a few features that many are not be aware of;  let's have a quick look at these features, and maybe they just might be what you have been looking for.
<!--more-->
## Hex preview 
Let's start off by having a look at hex preview.  Hex preview allows for the previewing of files in the package, and it can be found at https://preview.hex.pm.  Select the package you want to preview and that's it.  The file being previewed can be changed by selecting the appropriate file from the dropdown.

This is useful if you just want to have a quick look at a file; maybe a documenation file, a config file, or just explore the code and share it.  It is just a URL, so you can share the preview with your team.

The link below previews the CHANGELOG.md file for `Ecto`.    

https://preview.hex.pm/preview/ecto/3.12.4/show/CHANGELOG.md

![preview](./images/preview.png)
![preview file selection](./images/preview_selection.png)

## Hex diff
The next feature to have a look at is hex diff.  Hex diff can be found at https://diff.hex.pm and allows you to visually see the differences between two releases.  

As an example, let's have a look at the differences between `Ecto 3.1.1.2` and `Ecto 3.1.1.4`.  Just navigate to https://diff.hex.pm and select the package you want to see a diff on.  In this example, `Ecto` will be the package and the diff will be between versions 3.12.2 and 3.12.4.

![diff selection](./images/diff_selection.png)

Ta-da, pretty nice right?  What makes this even better is that it's just a URL so you can share it (just like hex preview).  Here is the URL for the example above.  

https://diff.hex.pm/diff/ecto/3.12.2..3.12.4  


## Hex outdated
Hex outdated is a `mix` task that can be run to show which packages are outdated in a project and which of those outdated packages can be updated.  

To try this out, you only need to enter `mix hex.outdated` in the terminal for your project.  The output from the sample project below shows which packages are able to be updated and which are not.

```bash
❯ mix hex.outdated
Dependency           Current     Latest      Status               
bandit               1.5.7       1.5.7       Up-to-date           
bcrypt_elixir        3.1.0       3.2.0       Update possible      
bulma                0.9.4       1.0.2       Update not possible  
dart_sass            0.7.0       0.7.0       Up-to-date           
dns_cluster          0.1.3       0.1.3       Up-to-date           
ecto_autoslug_field  3.1.0       3.1.0       Up-to-date           
ecto_sql             3.12.0      3.12.1      Update possible      
esbuild              0.8.1       0.8.2       Update possible      
ex_aws               2.5.6       2.5.7       Update possible      
ex_aws_s3            2.5.4       2.5.5       Update possible      
floki                0.36.2      0.36.3      Update possible      
gettext              0.26.1      0.26.2      Update possible      
hackney              1.20.1      1.20.1      Up-to-date           
jason                1.4.4       1.4.4       Up-to-date           
phoenix              1.7.14      1.7.14      Up-to-date           
phoenix_ecto         4.6.2       4.6.3       Update possible      
phoenix_html         4.1.1       4.1.1       Up-to-date           
phoenix_live_reload  1.5.3       1.5.3       Up-to-date           
phoenix_live_view    1.0.0-rc.6  1.0.0-rc.7  Update possible      
postgrex             0.19.1      0.19.3      Update possible      
sweet_xml            0.7.4       0.7.4       Up-to-date           
swoosh               1.17.0      1.17.3      Update possible      
telemetry_metrics    1.0.0       1.0.0       Up-to-date           
telemetry_poller     1.1.0       1.1.0       Up-to-date
```
Hopefully you will find these features of hex useful now or in the near future.  Check back for more great articles!

