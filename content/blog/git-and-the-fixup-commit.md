---
title: "Git and the fixup commit"
author: "John Hidey"
date: 2022-08-20
tags: ["git"]
draft: true
---
So you have been working on the pull-request all weekend.  You are just 
about done and want to review your work one last time before creating
the pull-request and beginning the review process.  While you are 
reviewing, you notice that you have simple spelling error that you 
commited locally a few commits ago.  You don't want to be the one that
has the commit titled "Spelling error".  But what can you do about it now.<!--more-->

There are a few ways to prevent/correct this the problem of having to created the dreaded 
"Spelling mistake" commit, but before we look at them, let's have a 
detailed look at our repository and see what we actually have.  

```goat
   .-------.       .-.        .-.        .-. 
   | Start +<-----+ 1 +<-----+ 2 +<-----+ 4 |
   '-------'       '-'        '+'        '-' 
                               ^          
                               |         
                             .-+-.       .-.        .-.       .+. 
                             | 3 +<-----+ B +<-----+ 5 +<----+ C |
                             '---'       '-'        '-'       '-' 
```
