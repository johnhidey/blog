---
title: "Basic Data Types and Operations in Elixir"
date: 2021-02-20
tags: ["elixir"]
author: "John Hidey"
draft: false
---
Elixir's core datatypes are much like that of any other language.  It has
Integers, Floats, Strings, and Booleans just like most languages.  It also has 
Atoms, Lists, Tuples, and a few others of which we'll take a look at here in this 
article.<!--more-->

Let's get started by starting `iex` and looking at some of these datatypes.
If you have [Elixir](https://elixir-lang.org) installed, you can start `iex` 
in a terminal by typing `iex` and hitting enter. You will then have a terminal 
which more or less looks like the below.
```ex
iex> 
```
## Numbers
Let's start by looking at numbers, more specifically Integers and Floats.
Numbers support the expected operations like `+`, `-`, `*`, and `/`. Trying a
few below we see that Integers used with `+`, `-`, and `*` return integers just
as expected, but when used with `/`, a float is returned.  Elixir always returns 
floats for the `/` operation.
```ex
iex> 6 + 2
8
iex> 6 - 2
4
iex> 6 * 2
12
iex> 6 / 2
3.0
```
For integer division, you will need to use the `div` function.  The `div` 
function will always return just the integer part of the division.  If you are
interested in the remainder, use the `rem` function.  
```ex
iex> div(6, 2)
3
iex> rem(6, 2)
0
iex> rem(6, 4)
2
```
## Strings
Elixir strings are always delimited by double quotes and are encoded as UTF-8.
```ex
iex> "Hello, Welcome to Elixir"  
"Hello, Welcome to Elixir"
```
String concatenation in Elixir is not done with the `+` operator, but rather
the `<>` operator.  Let's look at an example.
```ex
iex> "Hello " <> "World"
"Hello World"
iex> "Hello" <> " " <> "World"
"Hello World"
```
You can also combine strings using interpolation in Elixir using the `#{}` 
syntax within a string.
```ex
iex> world = "World"
"World"
iex> "Hello #{world}"
"Hello World"
```
## Atoms
Atoms are basically a constant whose value is the text of the atom.  Atoms are
used to specify the type/behavior of some other type.  
```ex
iex> :ok
:ok
iex> {:ok, 12}
{:ok, 12}
iex> {:error, "Something went wrong"}
{:error, "Something went wrong"}
```
In the example above, an atom could be used to return the state of a return 
value like a successful return of `{:ok, 12}` or an unsuccessful return of
`{:error, "Something went wrong"}`.
## Booleans
Elixir has booleans for `true` and `false`.  One interesting thing about 
booleans in Elixir is that they are actually atoms.  The atom `:true` is equal
to the boolean `true`. Below will show this and hopefully make it easier to 
understand.
```ex
iex> true
true
iex> :true
true
iex> true == :true
true
iex> not :true
false
```
## Lists
List in Elixir use `[]` to denote them. It is important to note that they are 
not arrays. In fact, they are implemented internally as linked lists.  The 
list can be of any type and can have many types in the same list.  One other 
important thing to note about list and all of Elixir for that matter is that 
list is immutable.  Any operation on a list will never modify the list, but 
instead return a new list.
```ex
iex> [1,2,3]
[1, 2, 3]
iex> list = [1,2,3]
[1, 2, 3]
iex> hd(list)
1
iex> tl(list)
[2, 3]
iex> list
[1, 2, 3]
iex> list ++ [4,5]
[1, 2, 3, 4 ,5]
iex> list
[1, 2, 3]
iex> list -- [2]
[1, 3]
```
## Tuples
Tuples are like lists in the sense that they can hold a value of any type. 
A Tuples' elements are stored in contigeous memory making it very easy and fast
to lookup the value of a given element by index.
```ex
iex> tuple = {:ok, "Hello World"}
{:ok, "Hello World"}
iex> elem(tuple, 1)
"Hello World"
```
Tuples are often used where an atom is the first element of the tuple and state
type/behavior. 
```ex
iex> {:status, :success}
{:status, :success}
iex> {:error, "Something went wrong"}
{:error, "Something went wrong"}
```
## Summary
After looking at some of the datatypes in Elixir, you can see that it isn't 
much different in it's datatype support than any other language you are 
familiar with.

In the next post, we'll have a brief look at pattern matching and the match
operator in Elixir.  
