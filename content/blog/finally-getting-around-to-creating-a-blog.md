---
title: "Finally getting around to creating a blog"
date: 2021-02-18
author: "John Hidey"
draft: false
---
Finally taking the time to get a blog up and running for myself.  I have been taking for 
years about putting up a blog so that share my thoughts, ideas and questions with the 
programming community.<!--more-->  

I created this blog with [Hugo](https://gohugo.io/), an awesome static site generator and DigitalOcean's [App Platform](https://www.digitalocean.com/products/app-platform/).
If you would like to try it out click the getting started with DigitalOcean link below which will 
give you $100 of credit on DigitalOcean for the next 60 days.

[Get started with DigitalOcean](https://m.do.co/c/d500322ca1a5)

So now that the blog is up and running, I hope that you will follow along and help/correct me
where I may go wrong.  This experience of writing about what I may be working on is 
quite new to me and I hope I can learn from this experience.

