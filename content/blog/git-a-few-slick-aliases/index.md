---
title: "A few slick git aliases"
author: "John Hidey"
date: 2022-09-29
tags: ["git"]
---
If you're writing code, you are almost certain to be using git.  One of
the awesome features of git is the ability to add your own aliases to
help save a few keystrokes, or even add some slick functionality.
Well, here a few aliases that are pretty slick. Check'em out.  <!--more-->

The first one uses [gitignore.io][fzf] to help create those `.gitignore` 
files relatively quick.  With this alias, you can either create a new 
.gitignore file to append to an existing one.  

```sh 
[~/project]$ git alias.gi = "!gi() { \
  curl -sL https://www.toptal.com/developers/gitignore/api/$@; }; \
  gi"

[~/project]$ A few ways to use it.

[~/project]$ # Create a new file 
[~/project]$ git gi linux,windows,macos,elixir,erlang > .gitignore 


[~/project]$ # Append to an existing one
[~/project]$ git gi node,visualstudiocode >> .gitignore 

``` 

The second alias that is very useful is for git fixup commits.
Every now and then you have that commit that really belongs to 
another commit you few a few back.  Well, a fixup commit allows for 
that.  You need to reference the commit SHA1 hash of 
the commit you want your new commit to be part of.

This means looking at the git log for the right commit hash.  Not bad,
but can be a bit cumbersome at times, so have a look at what this 
alias can help you with.


```sh 
[~/project]$ git alias.fixup = "!git log -n 20 --pretty=format:'%h %s' \
  --no-merges | fzf | cut -c -7 | \
  xargs -o git commit --fixup"

[~/project]$ # The old way of doing it

[~/project]$ git log
04a5c19 - (HEAD -> testbranch) An empty commit (12 minutes ago) 
bae704b - (gitlab/master, master) Change title to use lowercase l for let's. 

[~/project]$ # With the new alias 

[~/projects] git commit --followup 04a5c19 

```
![Screenshot of git fixup](./images/fzf.png)
When the fzf diplays the 20 most recent commits, you can filter them 
by simply typing to search within the comments.  Once you you have the
one selected you want, just simply hit enter.


Hope these two aliases help you out. 

[fzf]: https://gitignore.io
