---
title: "Developers daily Git commands"
date: 2022-04-07
author: "John Hidey"
tags: ["git"]
draft: false
---
As a software engineer, using a Version Control System (VCS) is pretty much a given and these days most of the software engineers/developers out there lean towards [Git][git].  Let's have a look at a few of the git commands that you might find yourself using on a daily basis.  These commands will not be presented in any given order/workflow, they are simply the commands that you will find yourself using on a regular basis.<!--more-->

So lets get started with the first command that you'll most likely be using on a daily basis.  

### git clone 
Imagine this, after starting at a new  job and after all that paperwork, your teammates tell you to pull down the project hosted on Github or Gitlab.  What do you do?  Well, it's pretty straight forward.  For more information, see the documentation [here][clone]

```bash
# Cloning a repository
# git clone <repo-url>

# clone the repo via https
$ git clone https://gitlab.com/johnhidey/blog.gitlab

# clone the repo via SSH 
$ git clone git@gitlab.com:johnhidey/blog.git 
```
### git status
This one is pretty much exactly what you think it is.  It reports to you the status of a repo telling what things like, what branch are you on, what files are staged (added to the index), what files have been modified and lastly, you are also told if there are any files in your directory which git is not tracking. For more information, see the documentation [here][status]
```bash
# Git status 

$ git status 
On branch feature-branch
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)

      new file: .gitignore

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)

      modified: README.md 

Untracked files:
  (use "git add <file>..." to include in what will be committed)

      .tool-versions
```

### git checkout
Git checkout allow you to change from one branch to another and update your working tree while doing so.  A branch is simply just another commit and your working tree is the files you work with which are associated with your repository.    

Checkout also has another nice feature of it that allows you to create a new branch from the current branch and change to it all within a single command. For more information see the documentation [here][checkout]
```bash
# Git checkout

# Check out a branch named master
$ git checkout master

# While on the master branch, create a new branch feature-branch
# from the master branch and change to the newly created branch
$ git checkout -b feature-branch 
```

### git add
Add is arguably the most frequently used git command there is.  It simply does one thing, and that is add to the index.  The index is a collection of changes to be committed.  

When you make a change(s) which you want to commit, you need to add them to the index.  Git add does just this by passing it a [pathspec][pathspec] telling it which file(s) to add to the index. For more information see the documentation [here][add] 
```bash
# Git add

# add file file.txt in current directory to index
$ git add file.txt

# add all files and directories of the current directory
$ git add .
```

### git commit    
When all the changes to a file, directory or combination of them are complete, the changes are going to need to be recorded.  The recording of these changes in the process of committing into the git repository.  Changes which have not been added to the index will NOT be committed.  Each commit is an entry in a ledger and records not only the changes just committed, but also the history of all changes. When committing changes, git will prompt for a commit message which will be stored with that commit to aide in identifying what changes are in that commit.  For more information see the documentation [here][commit]
```bash
# Git commit 

# git commit with a single line commit message"
$ git commit -m "Single line commit message"

# git commit opening editor and allowing for complex commit message
$ git commit 
```

### git pull
After arriving at the office in the morning, a good practice might be to integrate changes from the main branch into the the feature branch which you are currently working on.  For more information see the documentation [here][pull]
```bash
# Git pull

$ git pull
```

### git log
Just like the status command described above, git log also gives insight into the repository and all the changes which have been made to it.  Earlier a commit was described as ledger entry and a collection of commits forms the log.  Git log is a means of viewing all the commits from everyone on the team and commit messages.  Git log lists the commit hash, author, date and commit message.  It can also be shown as a graph giving the relationship between all the commits.  It should be noted that it is very common to create a git alias for log with all your preferred options.  i.e. `git alias --global alias.hist 'log --oneline --abbrev-commit --decorate --graph'`.  For more information see the documentation [here][log]
```bash
# Git log

# Most basic form of git log
$ git log 

# A single line version of each commit
$ git log --oneline

# A more complex git log showing the output
$ git log --graph --oneline --abbrev-commit --decorate

* 6dc4039 (HEAD -> git, origin/master, origin/HEAD, master) Added file c.txt
* e653c7d Updated file b.txt
* 9b884a7 Updated file a.txt
*   0d2b150 Merge branch 'add-files' into 'master'
|\  
| * de605ce Added a few files
|/  
*   e73499f Inital commit'
```

### git push
Keeping commits local doesn't provide much redunduncy or protect from lose of code.  This is why most projects will have "central" repository where all commits can come together.  The act of taking local commits and putting them into the "central" repository is called pushing.  Invoking git push on a local branch will likely result in git prompting you to run `git push --set-upstream origin local-feature` to configure the local branch with an upstream branch. Once this has been done once, it usually isn't required after that.  For more information see the documentation [here][push]
```bash
# Git push

# Git push without options
$ git push

# Git push specifying remote and branch
$ git push origin feature-branch
```

### git branch
To list, create and delete branches, reach for git branch command.  Not much explaination is needed here for such a basic command.  For more information see the documentation [here][branch]
```bash
# Git branch

# git branch without options
$ git branch 

# git branch with verbose listing 
$ git branch -v 
```

### git merge
Merging is the act of bring multiple commit collections together.  Merge is one of those beauties of git which allow for a developer to work completely isolated and then at the developers request, take the changes from the feature brnach and merge them into some other branch.  For more information see the documentation [here][merge]
```bash
# Git merge

# On master branch currently and will merge into master, the feature-branch 
$ git merge feature-branch 
```

This was just a brief look at a few of the more common commands that developers will use with git on a daily basis.  If you are looking for more detailed information, please visit the documentation page [here][docs] or use the `man` from the terminal.

GIT-R-Done now.


[git]: https://www.git-scm.com/ 
[docs]: https://git-scm.com/docs/
[clone]: https://git-scm.com/docs/git-clone
[status]: https://git-scm.com/docs/git-status
[checkout]: https://git-scm.com/docs/git-checkout
[add]: https://git-scm.com/docs/git-add
[commit]: https://git-scm.com/docs/git-commit
[pull]: https://git-scm.com/docs/git-pull
[log]: https://git-scm.com/docs/git-log
[push]: https://git-scm.com/docs/git-push
[branch]: https://git-scm.com/docs/git-branch
[merge]: https://git-scm.com/docs/git-merge
[pathspec]: https://git-scm.com/docs/gitglossary#Documentation/gitglossary.txt-aiddefpathspecapathspec
