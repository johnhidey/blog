---
title: "The Match Operator in Elixir"
date: 2021-02-24
author: "John Hidey"
tags: ["elixir"]
draft: false
---
In the last post we took a brief look at the different datatypes in Elixir, and
in this post we're going to take a look at the match `=` operator.  The match
operator is often mistakenly called an assignment which isn't quite right, 
let's have a look at the match operator and see if we can begin to understand 
it. <!--more-->
```ex
iex> x = "a"
"a"
iex> x
"a"
```
Right now you are saying, what's the big deal, it's an assignment statement.
If the above was all it did, you would be right.  So let's try something
different. Put the `"a"` on the left side of the match operator and the 
variable `x` on right and let's see what happens.
```ex
iex(1)> x = "a"
"a"
iex(2)> "a" = x
"a"
iex(3)> "b" = x
** (MatchError) no match of right hand side value: "a"
```
So what happened here is that the match operator tries to make the left side
and the right side equal.  In the first expression `"a" = x` it succeeds and 
returns the match of `"a"`. In the second example with `"b" = x` it will fail
cause there is nothing it can do to make the left and right side of the match
equal.

This idea can be used with all the datatypes in Elixir, and when pattern 
matching with more complex datatypes, you can deconstruct a complex type.  

Let's have a look at an example.
```ex
iex> api_result = {:ok, "Some good data here"}
{:ok, "Some good data here"}
iex> {:ok, data} = api_result
{:ok, "Some good data here"}
iex> data
"Some good data here"
```
In the example above, the match operator sees that it can set the variable data
to the value of `"Some good data here"` and that will make both sides of the 
operator equal, a match was found.

Here is another example using a list and we'll deconstruct it into the head 
and tail which is something that you will see often in Elixir.  
```ex
iex> list = ["red", "white", "blue"]
["red", "white", "blue"]
iex> [head | tail] = list
["red", "white", "blue"]
iex> head
"red"
iex> tail
["white", "blue"]
```
At last but not least, we'll finish with an example of the match operator 
using a combination of the last two examples.
```ex
iex> flag_colors = ["red", "white", "blue"]
["red", "white", "blue"]
iex> api_result = {:ok, flag_colors}
{:ok, ["red", "white", "blue"]}
iex> {status, [first_color | rest_of_colors]} = api_result
{:ok, ["red", "white", "blue"]}
iex> status
:ok
iex> first_color
"red"
iex> rest_of_colors 
["white", "blue"]
iex> {status, _} = api_result
{:ok, ["red", "white", "blue"]}
iex> status 
:ok
```
Above are a few more samples where the match operator will assign variables to
make both sides of the operator equal.  There are times where you don't care
about the data structure as a whole, you just want one piece of information from
it.  To do this, build a match on the left side just like before, but where you
are not concerned with the value you can put a `_` which will ignore that 
value.  

The last example above shows this. With the `_` you may also want to
know what that value represents in which case you can put the `_` in front of
a variable name.  Again, it will be ignored, but the variable name gives you 
information about what is being ignored.

The match operator is a very powerful operator and is used quite often in 
Elixir.  Pattern matching in Elixir wouldn't be possible without the match
operator.  As you continue, your knowledge of the match operator will continue 
to develop and be used quite extensively in your programs.
